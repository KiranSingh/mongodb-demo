using System;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Moq;
using UsersApi.Controllers;
using UsersApi.Entities;
using UsersApi.Mappings;
using UsersApi.Models;
using Xunit;

namespace UsersApi.UnitTests.Controller
{
    public class UserControllerTests : IClassFixture<MongoTestFixture>
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly UsersController _controller;
        private readonly User _user;
        private readonly IMapper _mapper;
        private readonly IMongoCollection<User> _collection;

        public UserControllerTests(MongoTestFixture testFixture)
        {
            testFixture.Users.DeleteMany(FilterDefinition<User>.Empty);
            _user = _fixture.Create<User>();
            testFixture.Users.InsertOne(_user);
            _collection = testFixture.Users;
            
            var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<UserProfile>());
            _mapper = mapperConfiguration.CreateMapper();
            _controller = new UsersController(testFixture.Database, _mapper);
        }

        [Fact]
        public async Task Get_ValidId_ReturnsExpectedResult()
        {
            // Arrange
            var expected = _mapper.Map<User, UserModel>(_user);

            // Act
            var actual = await _controller.Get(_user.Id);

            // Assert
            actual.As<OkObjectResult>()
                .Value.Should().BeEquivalentTo(expected,
                    opt => opt.SetDateTimeEquivalence());
        }

        [Fact]
        public async Task Get_InvalidId_ReturnsBadRequestResult()
        {
            // Arrange
            var id = Guid.NewGuid();
            
            // Act
            var actual = await _controller.Get(id);

            // Assert
            actual.As<BadRequestObjectResult>().Value.Should().Be($"No user exists for Id: {id}");
        }

        [Fact]
        public async Task Post_ValidModel_AddsExpectedUser()
        {
            // Arrange
            var loginModel = _fixture.Create<LoginModel>();
            var expected = _mapper.Map<LoginModel, User>(loginModel);

            // Act
            var actual = (await _controller.Post(loginModel)).As<CreatedAtActionResult>();

            // Assert
            actual.Should().NotBeNull();
            actual.ActionName.Should().Be(nameof(UsersController.Get));
            var id = actual.RouteValues["id"].As<Guid>();
            
            var user = await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();
            user.Id.Should().NotBeEmpty();
            BCrypt.Net.BCrypt.Verify(loginModel.Password, user.PasswordHash).Should().BeTrue();
            actual.Value.Should().BeEquivalentTo(expected, 
                opt => opt
                    .Excluding(x => x.Id)
                    .Excluding(x => x.PasswordHash)
                    .SetDateTimeEquivalence());
        }
        
        [Fact]
        public async Task Post_ModelStateInvalid_ReturnsBadRequest()
        {
            // Arrange
            var expected = _fixture.Create<string>();
            _controller.ModelState.AddModelError("some-model", expected);

            // Act
            var actual = await _controller.Post(new Mock<LoginModel>().Object);

            // Assert
            actual.As<BadRequestObjectResult>()
                .Value.As<SerializableError>()["some-model"].As<string[]>()[0].Should().Be(expected);
        }
    }
}