using System;
using Mongo2Go;
using MongoDB.Driver;
using UsersApi.Entities;

namespace UsersApi.UnitTests
{
    public class MongoTestFixture : IDisposable
    {
        private readonly MongoDbRunner _runner;

        public MongoTestFixture()
        {
            _runner = MongoDbRunner.Start(singleNodeReplSet: false);
            var client = new MongoClient(_runner.ConnectionString);
            Database = client.GetDatabase("IntegrationTest");
            Users = Database.GetCollection<User>(nameof(Users));
        }
        
        public IMongoDatabase Database { get; }
        
        public IMongoCollection<User> Users { get; }
        
        public void Dispose() => _runner?.Dispose();
    }
}