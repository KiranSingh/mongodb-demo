using System.ComponentModel.DataAnnotations;
using System.Reflection;
using FluentAssertions;
using UsersApi.Models;
using Xunit;

namespace UsersApi.UnitTests.Models
{
    public class LoginModelTests
    {
        [Fact]
        public void UserName_RequiredAttribute_Present()
        {
            // Arrange // Act // Assert
            typeof(LoginModel)
                .GetProperty(nameof(LoginModel.Username))
                .GetCustomAttribute<RequiredAttribute>()
                .Should().NotBeNull();
        }

        [Fact]
        public void Password_RequiredAttribute_Present()
        {
            // Arrange // Act // Assert
            typeof(LoginModel)
                .GetProperty(nameof(LoginModel.Password))
                .GetCustomAttribute<RequiredAttribute>()
                .Should().NotBeNull();
        }

        [Fact]
        public void Password_StringLengthAttribute_PresentWithExpectedProperties()
        {
            // Arrange // Act 
            var actual = typeof(LoginModel)
                .GetProperty(nameof(LoginModel.Password))
                .GetCustomAttribute<StringLengthAttribute>();

            // Assert
            actual.Should().NotBeNull();
            actual.ErrorMessage.Should().Be("Password of 5 to 20 characters required");
            actual.MaximumLength.Should().Be(20);
            actual.MinimumLength.Should().Be(5);
        }
    }
}