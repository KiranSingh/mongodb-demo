using System;
using AutoFixture;
using AutoMapper;
using FluentAssertions;
using UsersApi.Entities;
using UsersApi.Mappings;
using UsersApi.Models;
using Xunit;

namespace UsersApi.UnitTests.Mappings
{
    public class UserProfileTests
    {
        private readonly Fixture _fixture = new Fixture();
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _mapperConfiguration;

        public UserProfileTests()
        {
            _mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<UserProfile>());

            _mapper = _mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void Mappings_Configuration_Valid()
        {
            // Arrange // Act // Assert
            _mapperConfiguration.AssertConfigurationIsValid();
        }

        [Fact]
        public void Map_LoginModelToUser_ResultAsExpected()
        {
            // Arrange
            var model = _fixture.Create<LoginModel>();
            var nowTime = DateTime.Now;

            // Act
            var actual = _mapper.Map<LoginModel, User>(model);

            // Assert
            actual.Created.Should().BeCloseTo(nowTime);
            actual.Id.Should().NotBeEmpty();
            actual.LastActive.Should().BeCloseTo(nowTime);
            BCrypt.Net.BCrypt.Verify(model.Password, actual.PasswordHash).Should().BeTrue();
            actual.Username.Should().Be(model.Username);
        }

        [Fact]
        public void Map_UserToUserModel_ResultAsExpected()
        {
            // Arrange
            var user = _fixture.Create<User>();
            var now = DateTime.Now;

            // Act
            var actual = _mapper.Map<User, UserModel>(user);

            // Assert
            actual.Created.Should().BeCloseTo(user.Created);
            actual.LastActive.Should().BeCloseTo(user.LastActive);
            actual.Username.Should().Be(user.Username);
        }
    }
}