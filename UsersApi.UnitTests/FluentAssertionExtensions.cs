using System;
using FluentAssertions;
using FluentAssertions.Equivalency;

namespace UsersApi.UnitTests
{
    public static class FluentAssertionExtensions
    {
        public static EquivalencyAssertionOptions<T>
            SetDateTimeEquivalence<T>(this EquivalencyAssertionOptions<T> options, int precision = 1000) => 
            options
            .Using<DateTime>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation, precision))
            .WhenTypeIs<DateTime>();
    }
}