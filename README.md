# Mongo DB Demo #

### A limited API to demo the usage of Mongo DB ###

* Mongo DB instance is required - the URL is currently hard coded in `Startup.cs` 
* .Net Core SDK version 3.1 will be needed to run the project.

* The API is meant to conform to [HATEOS](https://en.wikipedia.org/wiki/HATEOAS)
* [Mongo2Go](https://github.com/Mongo2Go/Mongo2Go) library used to implement MongoDB integration tests
* [AutoFixture](http://autofixture.github.io/) library used to generate dummy data for unit tests
* [AutoMapper](http://automapper.org/) library used to map models
* [Fluent Assertions](http://fluentassertions.com) library used for validating tests