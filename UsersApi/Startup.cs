using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;
using UsersApi.Models;

namespace UsersApi
{
    public class Startup
    {
        private const string MongoConnectionString = "mongodb://localhost:27017/UsersTest";
        
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IMongoClient>(sp => new MongoClient(MongoConnectionString));

            services.AddScoped<IMongoDatabase>(sp =>
            {
                var mongoUrl = MongoUrl.Create(MongoConnectionString);

                var mongoDatabase = sp.GetService<IMongoClient>()
                    .GetDatabase(mongoUrl.DatabaseName,
                        new MongoDatabaseSettings
                        {
                            GuidRepresentation = GuidRepresentation.Standard,
                        });
                return mongoDatabase;
            });
            
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Users API";
                };
            });
            
            services.AddAutoMapper(typeof(UserModel));
            
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IMapper mapper)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            mapper.ConfigurationProvider.AssertConfigurationIsValid();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}