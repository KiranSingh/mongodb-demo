using System;

namespace UsersApi.Models
{
    public class UserModel
    {
        public DateTime Created { get; set; }
        
        public DateTime LastActive { get; set; }
        
        public string Username { get; set; }
    }
}