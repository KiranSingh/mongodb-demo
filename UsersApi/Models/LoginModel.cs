using System.ComponentModel.DataAnnotations;

namespace UsersApi.Models
{
    public class LoginModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Password of 5 to 20 characters required")]
        public string Password { get; set; }
    }
}