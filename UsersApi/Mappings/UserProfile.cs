using System;
using AutoMapper;
using BCrypt.Net;
using UsersApi.Entities;
using UsersApi.Models;

namespace UsersApi.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<LoginModel, User>()
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.LastActive, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.PasswordHash,
                    opt => opt.MapFrom(src => BCrypt.Net.BCrypt.HashPassword(src.Password, SaltRevision.Revision2B)))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username));
            
            CreateMap<User, UserModel>();
        }
    }
}