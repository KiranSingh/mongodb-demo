using MongoDB.Driver;

namespace UsersApi.Extensions
{
    public static class MongoExtensions
    {
        public static IMongoCollection<T> Collection<T>(this IMongoDatabase database) =>
            database.GetCollection<T>($"{typeof(T).Name}s");
    }
}