using System;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using UsersApi.Entities;
using UsersApi.Extensions;
using UsersApi.Models;

namespace UsersApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IMongoCollection<User> _collection;
        private readonly IMapper _mapper;

        public UsersController(IMongoDatabase database, IMapper mapper)
        {
            _mapper = mapper;
            _collection = database.Collection<User>();
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var user = await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();

            if (user == null)
                return BadRequest($"No user exists for Id: {id}");

            return Ok(_mapper.Map<User, UserModel>(user));
        }

        [HttpPost]
        public async Task<IActionResult> Post(LoginModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
                
            await _collection.InsertOneAsync(_mapper.Map<LoginModel, User>(model));

            var user = await _collection.Find(x => x.Username.ToLower() == model.Username.ToLower())
                .SingleOrDefaultAsync();

            return user == null
                ? StatusCode((int) HttpStatusCode.InternalServerError, "There was a problem creating the user")
                : CreatedAtAction(nameof(Get), new {id = user.Id}, _mapper.Map<User, UserModel>(user));
        }
    }
}