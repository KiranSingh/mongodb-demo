using System;
using MongoDB.Bson.Serialization.Attributes;

namespace UsersApi.Entities
{
    public class User
    {
        [BsonId]
        public Guid Id { get; set; }
        
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime Created { get; set; }
        
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastActive { get; set; }
        
        public string PasswordHash { get; set; }
        
        public string Username { get; set; }
    }
}